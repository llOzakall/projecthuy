<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Add Page</title>
  </head>
  <body>

    <?php 
        include 'extension.php'; 
        include 'connect.php';
    ?>
        <div class="container">
            <h2 class="text-center my-5">MY CRUD PHP AND BOOTSTRAP + SWEETALERT2</h2>
            <hr>
            <div class="row justify-content-center">
                <div class="col-lg-5">
                    <div class="card shadow border-0">
                    <h3 class="card-header text-center">หน้าเพิ่มข้อมูล</h3>
                        <div class="card-body px-4">
                            <form action="form_add_db.php" method="post">
                                <div class="row g-3">
                                    <div class="col-12">
                                        <label class="form-label">Name :</label>
                                        <input type="text" class="form-control" id="names"  name="names" required>
                                    </div>
                                    <div class="col-12">
                                        <label class="form-label">Mode :</label>
                                        <input type="text" class="form-control" id="mode" name="mode" required>
                                    </div>
                                    <div class="col-12">
                                        <label class="form-label">Price :</label>
                                        <input type="text" class="form-control" id="price" name="price" required>
                                    </div>
                                    <a onclick = "Addmodel()" class="btn btn-primary d-block w-75 mx-auto" >เพิ่มข้อมูล</a>
                                </div>
                            </form>
                        </div>
                    </div>           
                </div>
            </div>
        </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    -->
  </body>
</html>

<script type="text/javascript">
    function Addmodel() {
        var names = $('#names').val();
        var mode = $('#mode').val();
        var price = $('#price').val();
        $.ajax({
            url: "form_add_db.php",
            type: "POST",
            data: {
                names : names,
                mode : mode,
                price : price,
            },
            success: function(data) {
                if(data == 'pass') {
                    Swal.fire({
                        icon: 'success',
                        title: 'แก้ไขข้อมูลสำเร็จ',
                        timer: 3000
                    }).then((result) => {
                        window.location.href = 'form_list.php';
                    })
                }
            }
        });
}





</script>
