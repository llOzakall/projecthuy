<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Add Page</title>
  </head>
  <body>
        <?php
            include 'connect.php';
            include 'extension.php';
            $id = $_GET['id'];
            $sql = sprintf("SELECT * FROM game_list WHERE id = '%s'", $id);

            $result = mysqli_query($conn, $sql) or die ("Error in sql : $sql" . mysqli_error($conn));
            $rows = mysqli_fetch_array($result);
        ?>


        <div class="container">
            <h2 class="text-center my-5">MY CRUD PHP AND BOOTSTRAP + SWEETALERT2</h2>
            <hr>
            <div class="row justify-content-center">
                <div class="col-lg-5">
                    <div class="card shadow border-0">
                    <h3 class="card-header text-center">แก้ไขข้อมูล</h3>
                        <div class="card-body px-4">
                            <form action="form_edit_db.php" method="post">
                                <div class="row g-3">
                                <div class="col-12">
                                        <label class="form-label">id :</label>
                                        <input type="text" class="form-control" name="id" value = <?php  echo $rows['id']; ?> readonly required>
                                    </div>
                                    <div class="col-12">
                                        <label class="form-label">Name :</label>
                                        <input id="names" type="text" class="form-control" name="names" value = <?php  echo $rows['names']; ?>  required>
                                    </div>
                                    <div class="col-12">
                                        <label class="form-label">Mode :</label>
                                        <input id="mode" type="text" class="form-control" name="mode" value = <?php  echo $rows['mode']; ?>  required>
                                    </div>
                                    <div class="col-12">
                                        <label class="form-label">Price :</label>
                                        <input id="price" type="text" class="form-control" name="price" value = <?php  echo $rows['price']; ?> required>
                                    </div>
                                    <a  class="btn btn-primary d-block w-75 mx-auto" onclick="Editmodel(<?= $rows['id'] ?>)">แก้ไขข้อมูล</a>
                                </div>
                            </form>
                        </div>
                    </div>           
                </div>
            </div>
        </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    -->
  </body>
</html>

<script type="text/javascript">
    function Editmodel(id){
        var names = $('#names').val();
        var mode = $('#mode').val();
        var price = $('#price').val();
        Swal.fire({
                        title: 'แน่ใจนะว่าจะแก้ไข???',
                        text: 'คุณจะไม่สามารถกลับมาดูได้อีก!',
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'ยืนยัน',
                        cancelButtonText: 'ยกเลิก'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            $.ajax({
                                url: "form_edit_db.php",
                                method: "POST",
                                data: {
                                    id: id,
                                    names: names,
                                    mode: mode,
                                    price: price,
                                },
                                success: function(data) {
                                    if(data == 'pass'){
                                        Swal.fire({
                                            icon: 'success',
                                            title: 'แก้ไขข้อมูลสำเร็จ',
                                            showConfirmButton: false,
                                            timer: 3000
                                        }).then((result) => {
                                                window.location.href = 'form_list.php';
                                        });
                                   }
                                }
                            });
                        }
                    });
            }
</script>