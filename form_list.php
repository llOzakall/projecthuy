<?php
include 'configModelController.php';
 ?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->

 
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>
        <div class="container">
            <h2 class="text-center my-5">MY CRUD PHP AND BOOTSTRAP + SWEETALERT2</h2>
            <hr>
            <a href="form_add.php" class="btn btn-primary mb-3 ">เพิ่มข้อมูล</a>
            <table class="table table-dark table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Mode games</th>
                    <th>Price</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php $i=1;

                 foreach($result as $value) { ?>
                <tr class="table-active">
                    <td><?= $i++;  ?></td>
                    <td><?php echo $value['names'];  ?></td>
                    <td><?php echo $value['mode'];  ?></td>
                    <td><?php echo $value['price']." บาท";  ?></td>
                    <td>
                        <a href ="form_edit.php?id=<?= $value['id'] ?>" class="btn btn-warning btn-sm">แก้ไข</a>
                        <a onclick="Deletemodel(<?= $value['id'] ?>)" class="btn btn-danger btn-sm">ลบ</a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </div> 
        <?php mysqli_close($conn); ?>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    -->
  </body>
</html>
<script type="text/javascript">
    function Deletemodel(id){
        Swal.fire({
                        title: 'จะมาสมัครบางกอกเว็บอีกไหม???',
                        text: 'คุณจะไม่สามารถกลับมาดูได้อีก!',
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'ยืนยัน',
                        cancelButtonText: 'ยกเลิก'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            $.ajax({
                                url: "form_del.php",
                                method: "POST",
                                data: {
                                    id: id,
                                },
                                success: function(data) {
                                    if(data == 'pass'){
                                        Swal.fire({
                                            icon: 'success',
                                            title: 'ลบข้อมูลสำเร็จ',
                                            showConfirmButton: false,
                                            timer: 3000
                                        }).then((result) => {
                                                window.location.reload();
                                        });
                                   }
                                }
                            });
                        }
                    });
            }
</script>